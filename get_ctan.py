#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import urllib2, urllib, sys, re, os.path
from bs4 import BeautifulSoup as bs

base_url = 'http://www.ctan.org'

def gen_pkg_url(pkgname):
    url = base_url + '/pkg/' + pkgname
    return url

def get_page(url):
    res = urllib2.urlopen(url)
    return res.read()

def get_ctan_page(pkgname):
    url = gen_pkg_url(pkgname)
    html = get_page(url)
    return html

def get_version(html):
    ''' Given a CTAN package page(http://www.ctan.org/pkg/*), retrieve the package version'''
    sp = bs(html)
    table = sp.find('table', 'entry') # extract entry table
    try:
        # find <td>Ver-sion</td>, the version number is next element
        verstr = table.find('td', text=re.compile('^Ver')).next_sibling.string.strip()
    except AttributeError:
        print 'no version info found.'
        verstr = '0.0'
    
    m = re.search('^([0-9.]*)([a-zA-Z]*)', verstr)
    vermajor = m.group(1) # numeric part, major version
    verminor = m.group(2) # non-numeric minor version
    return verstr, vermajor, verminor

def get_desc(html):
    ''' Given a CTAN package page(http://www.ctan.org/pkg/*), retrieve the package description'''
    sp = bs(html)
    desc = sp.find('div', 'left').p.get_text()
    desc = desc.replace(u'\xad', '') # removing '\xad'
    return desc

def get_title(html):
    ''' Given a CTAN package page(http://www.ctan.org/pkg/*), retrieve the package title'''
    sp = bs(html)
    title = sp.title.string
    return title

def get_archive_url(html):
    ''' Given a CTAN package page(http://www.ctan.org/pkg/*), retrieve the url of the archive page'''
    sp = bs(html)
    table = sp.find('table', 'entry')
    # find <th>CTAN path</th>, the path link is next element
    th = table.find('th', text='CTAN path')
    if not th:
        # if <th>CTAN path</th> not found, try <td>Sources</td>
        th = table.find('td', text='Sources')
    linkstr = th.next_sibling.a['href']
    url = base_url + linkstr
    return url

def get_download_link(html):
    ''' Given a CTAN package page(http://www.ctan.org/pkg/*), retrieve the url of the zip file'''
    arv_url = get_archive_url(html)
    if re.search('\.sty$', arv_url): # direct link to sty file
        return arv_url
    arv_html = get_page(arv_url)
    sp = bs(arv_html)
    divright = sp.find('div', 'right') # find <div class="right">
    dllink = divright.find('a', text=re.compile('^Down'))['href']
    return dllink

def get_filesize(url):
    u = urllib2.urlopen(url)
    meta = u.info()
    filesize = int(meta.getheaders("Content-Length")[0])
    return filesize

def download_file(url, dwn_dir):
    file_name = url.split('/')[-1]
    u = urllib2.urlopen(url)
    f = open(os.path.join(dwn_dir, file_name), 'wb')
    meta = u.info()
    file_size = int(meta.getheaders("Content-Length")[0])
    print "Downloading: %s Bytes: %s" % (file_name, file_size)
    
    file_size_dl = 0
    block_sz = 8192
    while True:
        buffer = u.read(block_sz)
        if not buffer:
            break
        file_size_dl += len(buffer)
        f.write(buffer)
        status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8)*(len(status)+1)
        print status,
    print '' # inserting line break
    f.close()

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        pkg = sys.argv[1]
        html = get_ctan_page(pkg)
        ver = get_version(html)
        title = get_title(html)
        dllink = get_download_link(html)
        
        print title
        print 'Version:', ver
        print 'Download:', dllink
        res = raw_input('Download file? y/[n] ')
        if res == 'y':
            download_file(dllink, './')
    else:
        print 'usage: python ' + sys.argv[0] + ' search terms'




