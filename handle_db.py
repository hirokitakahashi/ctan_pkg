#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
import sqlite3, os, time
from types import *

def create_db(db_file):
    if os.path.isfile(db_file):
        print 'File already exists.'
        ret = raw_input('Replace? y/[n] ')
        if ret == 'y':
            os.remove(db_file)
        else:
            return
    con = sqlite3.connect(db_file)
    c = con.cursor()
    c.execute('create table pkg (name text, version text, last_mod text, files text, path text)')
    con.commit()
    con.close()
        
    
def find_pkg(db_file, name=None):
    ''' Returns entries if the package exists in the table, otherwise returns None'''
    con = sqlite3.connect(db_file)
    c = con.cursor()
    if not name: # show all
        c.execute('select * from pkg')
        res = c.fetchall()
    else:
        c.execute('select * from pkg where name=?', [name])
        res = c.fetchone()
    con.close()
    return res

def show_table_info(db_file):
    con = sqlite3.connect(db_file)
    c = con.cursor()
    c.execute('pragma table_info(pkg)')
    print c.fetchall()
    con.close()

def get_pkg_attr(db_file, name, *attr):
    '''Get package attributes. Attributes should be given as individual arguments, e.g. get_pkg_attr(name, 'version', 'last_mod').'''
    con = sqlite3.connect(db_file)
    c = con.cursor()
    sql = 'select ' + '%s, '*len(attr)
    sql = sql[:-2] + ' from pkg where name=?'
    sql = sql % attr
    c.execute(sql, [name])
    ret = c.fetchone()
    con.close()
    return ret

def add_pkg(db_file, name, version, last_mod, files, path):
    con = sqlite3.connect(db_file)
    c = con.cursor()
    entry = (name, version, last_mod, files, path)
    c.execute('insert into pkg values (?, ?, ?, ?, ?)', entry)
    con.commit()
    con.close()

def del_pkg(db_file, name):
    con = sqlite3.connect(db_file)
    c = con.cursor()
    c.execute('delete from pkg where name=?',[name])
    con.commit()
    con.close()

def update_pkg(db_file, name, version=None, last_mod=None, files=None, path=None):
    cols = []
    vals = []
    if version:
        cols.append('version')
        vals.append(version)
    if last_mod:
        cols.append('last_mod')
        vals.append(last_mod)
    if files:
        cols.append('files')
        vals.append(files)
    if path:
        cols.append('path')
        vals.append(path)
    
    if len(cols) == 0:
        return
    else:
        con = sqlite3.connect(db_file)
        c = con.cursor()
        sql = 'update pkg set ' + '%s=?, '*len(cols)
        sql = sql[:-2] + ' where name=?'# removing the tailing ,
        sql = sql % tuple(cols)
        print sql
        c.execute(sql, vals+[name])
        con.commit()
        con.close()
        
