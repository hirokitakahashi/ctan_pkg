#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import getopt, sys, zipfile, subprocess, re, time, shutil, os
import get_ctan as gc 
import handle_db as hd
from proc_conf import proc_conf

if sys.platform == 'win32':
    default_conf = os.path.expanduser('~/AppData/local/ctan_pkg/ctan_pkg.conf')
else:
    default_conf = os.path.expanduser('~/Library/ctan_pkg/ctan_pkg.conf')

def get_datetime():
    str = time.strftime('%Y-%m-%d %H:%M:%S')
    return str

def usage():
    msg = '''\
    usage: ctan_pkg [-hrugl] pkgname
    -h = show help message
    -i = initialise DB file
    -r = delete package
    -u = update an existing package
    -g = register an existing package
    -l = list installed packages
    '''
    print msg

def download_zip(info, conf):
    page = info.ctan_page
    ver, vermaj, vermin = gc.get_version(page)
    desc = gc.get_desc(page)
    title = gc.get_title(page)
    dllink = info.ctan_dllink
    filesize = gc.get_filesize(dllink)
    temp_dir = conf['temp_dir']
    
    print title
    print 'Version:', ver
    print 'Description:', desc
    print 'Download:', dllink
    print 'File size:', filesize/1000, 'kb'
    res = raw_input('Install package? y/[n] ')
    if res == 'y':
        gc.download_file(dllink, temp_dir)
        zfile = dllink.split('/')[-1]
        return zfile
    else:
        return None

def install_pkg(zfile, info, conf):
    '''Installing the package files into the local system. The files are normally downloaded as a zip file,
    which will be unzipped and expanded. However sometimes the downloaded file is just a single sty file.
    In this case the sty file is simply copied.'''
    
    temp_dir = conf['temp_dir']
    install_to = conf['install_to']
    db_file = conf['db_file']
    if re.search('\.sty$', zfile): # not zip, just sty file
        extzip = False
        install_dir = os.path.join(install_to, info.pkgname)
        extract_dir = install_dir
        if os.path.exists(install_dir):
            res = raw_input('%s exists. Replace? y/[n] '%install_dir)
            if res == 'y':
                shutil.rmtree(install_dir)
            else:
                print 'Installation suspended.'
                return
        os.mkdir(install_dir)
        shutil.copyfile(os.path.join(temp_dir, zfile), os.path.join(install_dir, zfile))
        files = [zfile]
    else: # zip file
        zf = zipfile.ZipFile(os.path.join(temp_dir, zfile))
        # check if a directory exists in the zip file and all is contained in it
        makedir = False
        files = zf.namelist()
        for f in files:
            if not f.find('/'):
                makedir = True
                break
        if makedir:
            extract_dir = os.path.join(install_to, info.pkgname)
            install_dir = extract_dir
        else:
            extract_dir = install_to
            install_dir = os.path.join(extract_dir, info.pkgname)
        zf.extractall(extract_dir)
    print 'Installing to %s' % install_dir
    for f in files:
        print os.path.join(extract_dir, f)
        if re.search('.ins$', f):
            ret = raw_input('*.ins file found. Do you want to latex this? y/[n]? ')
            if ret == 'y':
                print 'cd %s;latex' % install_dir
                p = subprocess.Popen(['latex', os.path.basename(f)], cwd=install_dir, stdout=sys.stdout)
                p.wait()
                if p.returncode:
                    print 'Installing %s failed. return code %d' % (f, p.returncode)
    
    files_str = '\n'.join(files)
    hd.add_pkg(db_file, info.pkgname, info.ctan_version[0], get_datetime(), files_str, install_dir)
    print 'Database entry added.'


def uninstall_pkg(info, conf):
    db_file = conf['db_file']
    print 'Following files in %s will be deleted.' % info.local_path
    p = subprocess.Popen('ls', cwd=info.local_path, stdout=subprocess.PIPE)
    pstdout = p.communicate()[0]
    print pstdout
    ret = raw_input('OK? [y]/n ')
    if ret != 'n':
        try:
            shutil.rmtree(info.local_path)
        except:
            print 'Unable to remove files.'
        else:
            hd.del_pkg(db_file, info.pkgname)
            print 'Database entry deleted'

def list_packages(conf):
    db_file = conf['db_file']
    res = hd.find_pkg(db_file)
    print 'name', 'version', 'last modified','path'
    for line in res:
        print line[0], line[1], line[2], line[4]

def update_pkg(info, conf):
    ctan_v, ctan_vmaj, ctan_vmin = info.ctan_version
    local_v, local_vmaj, local_vmin = info.local_version
    if ctan_v == local_v:
        print 'Package is up to date.'
        return
    elif ctan_vmaj > local_vmaj:
        ret = raw_input('Update is available. Do you want to update? [y]/n ')
        if ret != 'n':
            uninstall_pkg(info, conf)
            zfile = download_zip(info, conf)
            install_pkg(zfile, info, conf)
    elif ctan_vmaj == local_vmaj and ctan_vmin != local_vmin:
        print 'local version %s, CTAN version %s' % (local_v, ctan_v)
        ret = raw_input('Update to CTAN version? y/[n]')
        if ret == 'y':
            uninstall(info)
            zfile = download_zip(info, conf)
            install_pkg(zfile, info, conf)
            
    
class pkg_info:
    def __init__(self, db_file, pkgname):
        self.db_file = db_file
        self.pkgname = pkgname

    def get_local(self):
        if hd.find_pkg(self.db_file, self.pkgname):
            self.local_found = True
            v, lm, f, p = hd.get_pkg_attr(self.db_file, self.pkgname, 'version', 'last_mod', 'files', 'path')
            m = re.search('^([0-9.]*)([a-zA-Z]*)', v)
            vmajor = m.group(1) # numeric part, major version
            vminor = m.group(2) # non-numeric minor version
            self.local_version = (v, vmajor, vminor)
            self.local_last_mod = lm
            self.local_files = f
            self.local_path = p
        else:
            self.local_found = False
    
    def get_ctan(self):
        try:
            page = gc.get_ctan_page(self.pkgname)
        except HTTPError:
            self.ctan_found = False
            return
        else:
            self.ctan_found = True
            self.ctan_page = page
            self.ctan_dllink = gc.get_download_link(page)
            ver, vermaj, vermin = gc.get_version(page)
            self.ctan_version = (ver, vermaj, vermin)
            
    
def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hirugl")
    except getopt.GetoptError:
        usage()
        sys.exit(1)
    
    remove = False
    update = False
    conf = proc_conf(default_conf)
    db_file = conf['db_file']
    for o, a in opts:
        if o == '-h':
            usage()
            sys.exit()
        elif o == '-i':
            hd.create_db(db_file)
            sys.exit()
        elif o == '-r':
            remove = True
            break
        elif o == '-l':
            list_packages(conf)
            sys.exit()
        elif o == '-u':
            update = True
            break
    
    pkg = sys.argv[-1] # package name
    info = pkg_info(db_file, pkg)
    info.get_local() # find package in the database
    
    if remove:
        if not info.local_found: # not installed
            print 'package %s not installed.' % pkg
        else:
            uninstall_pkg(info, conf)
    elif update:
        info.get_ctan()
        update_pkg(info, conf)
    else:
        if not info.local_found: # if not installed
            info.get_ctan()
            if not info.ctan_found:
                print 'Package not found on CTAN!'
                sys.exit(1)
            else:
                zfile = download_zip(info, conf)
                if not zfile:
                    sys.exit(1)
                else:
                    install_pkg(zfile, info, conf)
        else:
            print 'package %s already installed. Use -u option to update' %pkg

if __name__ == '__main__':
    main()

