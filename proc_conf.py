#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
# Read a configuration file.
# Each configuration line is in the form of 'x = y'.
# They are stored in a dictionary as dic['x'] = y and returned. 

def proc_conf(fn):
    config = {}
    f = open(fn)
    for l in f.readlines():
        l = l.strip() # removing the tailing '\n'
        if l == '': # empty line
            continue
        
        str = l.split('#') #split by '#' for comment
        if len(str) != 2: # this doesn't include a comment
            str2 = str[0]
        else:
            if str[0] == '': # the entire line is a comment
                continue
            else:
                str2 = str[0]
        
        # process the config line
        str3 = str2.split() # split by white spaces
        if len(str3) != 3 or str3[1] != '=':
            print 'Warning unrecongnised config line: ', str2
        else:
            config[str3[0]] = str3[2]

    f.close()
    return config 
                
            
                
        
